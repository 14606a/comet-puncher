#include "angler.hpp"
#include "motor.hpp"
#include "main.h"
int speed = 200;
int pos;
void setAngleHigh(float dis){
	pos = (-2.56*(pow(10.0,-8.0))*pow(dis,4.0)) + (2.475*(pow(10,-4.0)))*(pow(dis,3.0)) - ((.069)*pow(dis,2.0)) + 6.23*dis - 65.13;
	if (dis == 60){
		pos = 124;
	}
	else if (dis == 84){
		pos = 130;
	}
	else if (dis = 36){
		pos = 79;
	}
	pros::lcd::print(2,"Pos: %d", pos);
	pros::lcd::print(1,"Dis: %d", dis);

	anglerMotor.move_absolute(pos,speed);
}	
void setAngleLow(float dis){
	pos = (6.13*(pow(10,-6.0))*pow(dis,4.0)) - (.0012*pow(dis,3.0)) + (.0447*pow(dis,2.0)) + 2.097*dis + 76.45;
	pros::lcd::print(2,"Set: %d", pos);
	int intDis = pos;
	pros::lcd::print(1,"Dis: %d", intDis);
	 if (dis == 60){
		pos = 190;
	}
	else if (dis == 84){
		pos = 167;
	}
	 else if (dis == 36){
		 pos = 164;
	 }
	anglerMotor.move_absolute(pos,speed);
}
void setAngle(int angle){
	anglerMotor.move_absolute(angle,speed);
}
