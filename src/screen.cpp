#include "screen.h"
#include "main.h"
#include "autonVars.hpp"
//Define Styles
lv_obj_t * notifLabel;
static lv_style_t redBlueButtonStyleRel;
static lv_style_t redBlueButtonStylePr;
static lv_style_t frontBackButtonStyleRel;
static lv_style_t frontBackButtonStylePr;
lv_obj_t * frontBackLabel = lv_label_create(lv_scr_act(), NULL);
lv_obj_t * frontBackButton = lv_btn_create(lv_scr_act(), NULL);

lv_obj_t * parkLabel = lv_label_create(lv_scr_act(), NULL);
void sendToScreen(char* Message){
	lv_label_set_text(notifLabel, Message);
	lv_obj_align(notifLabel, NULL, LV_ALIGN_IN_BOTTOM_LEFT, 0, 5);
}
static lv_res_t changePark(lv_obj_t * sw){
	if (auton::park){
		auton::park = false;
	}
	else{
		auton::park = true;
	}
	return LV_RES_OK; /*Return OK if the button is not deleted*/
}
static lv_res_t toggleColor(lv_obj_t * btn){
	uint8_t id = lv_obj_get_free_num(btn);
	auton::redBlue = auton::redBlue*-1;
	if (auton::redBlue == 1){
		redBlueButtonStyleRel.body.border.color = LV_COLOR_RED;
		redBlueButtonStylePr.body.border.color = LV_COLOR_RED;
		redBlueButtonStylePr.body.main_color = LV_COLOR_RED;
		redBlueButtonStyleRel.body.main_color = LV_COLOR_RED;
	}
	else if(auton::redBlue == -1){
		redBlueButtonStyleRel.body.border.color = LV_COLOR_BLUE;
		redBlueButtonStylePr.body.border.color = LV_COLOR_BLUE;
		redBlueButtonStylePr.body.main_color = LV_COLOR_BLUE;
		redBlueButtonStyleRel.body.main_color = LV_COLOR_BLUE;
	}
	lv_btn_set_style(btn, LV_BTN_STYLE_REL, &redBlueButtonStyleRel);
	lv_btn_set_style(btn, LV_BTN_STYLE_PR, &redBlueButtonStylePr);


	return LV_RES_OK; /*Return OK if the button is not deleted*/
}

static lv_res_t toggleFrontBack(lv_obj_t * btn){
	if (auton::frontBack){
		auton::frontBack = false;
		lv_label_set_text(frontBackLabel, "Back");
		frontBackButtonStyleRel.body.main_color = LV_COLOR_BLACK;
		frontBackButtonStyleRel.body.border.color = LV_COLOR_BLACK;
	}
	else{
		auton::frontBack = true;
		frontBackButtonStyleRel.body.main_color = LV_COLOR_WHITE;
		frontBackButtonStyleRel.body.grad_color = LV_COLOR_BLACK;
		frontBackButtonStyleRel.body.border.color = LV_COLOR_SILVER;lv_label_set_text(frontBackLabel, "Front");
	}
	lv_btn_set_style(frontBackButton, LV_BTN_STYLE_REL, &frontBackButtonStyleRel);
	lv_btn_set_style(frontBackButton, LV_BTN_STYLE_PR, &frontBackButtonStylePr);
	return LV_RES_OK; /*Return OK if the button is not deleted*/
}

void start_auton(){
	//Red Blue
	//Create Styles
	lv_obj_t * notifLabel = lv_label_create(lv_scr_act(), NULL);
	
	lv_style_copy(&redBlueButtonStyleRel, &lv_style_btn_rel);
	redBlueButtonStyleRel.body.main_color = LV_COLOR_RED;
	redBlueButtonStyleRel.body.grad_color = LV_COLOR_BLACK;
	redBlueButtonStyleRel.body.border.color = LV_COLOR_RED;
	redBlueButtonStyleRel.body.border.width = 1;
	redBlueButtonStyleRel.body.border.opa = LV_OPA_50;
	redBlueButtonStyleRel.body.radius = 0;

	lv_style_copy(&redBlueButtonStylePr, &redBlueButtonStyleRel);

	//Create Label for Button
	lv_obj_t * label = lv_label_create(lv_scr_act(), NULL);
	lv_label_set_text(label, "Swap Color");
	lv_obj_align(label, NULL, LV_ALIGN_IN_TOP_LEFT, 0, 5);

	//Create Button
	lv_obj_t * redBlueButton = lv_btn_create(lv_scr_act(), NULL);
	lv_cont_set_fit(redBlueButton, true, true); /*Enable resizing horizontally and vertically*/
	lv_obj_align(redBlueButton, label, LV_ALIGN_OUT_BOTTOM_LEFT, 0, 10);
	lv_obj_set_free_num(redBlueButton, 1);   /*Set a unique number for the button*/
	lv_btn_set_action(redBlueButton, LV_BTN_ACTION_CLICK, toggleColor);
	
	//Set Styles
	lv_btn_set_style(redBlueButton, LV_BTN_STYLE_REL, &redBlueButtonStyleRel);
	lv_btn_set_style(redBlueButton, LV_BTN_STYLE_PR, &redBlueButtonStylePr);

	//Front Back
	//Create Styles
	lv_style_copy(&frontBackButtonStyleRel, &lv_style_btn_rel);
	frontBackButtonStyleRel.body.main_color = LV_COLOR_WHITE;
	frontBackButtonStyleRel.body.grad_color = LV_COLOR_BLACK;
	frontBackButtonStyleRel.body.border.color = LV_COLOR_SILVER;
	frontBackButtonStyleRel.body.border.width = 1;
	frontBackButtonStyleRel.body.border.opa = LV_OPA_50;
	frontBackButtonStyleRel.body.radius = 0;

	lv_style_copy(&frontBackButtonStylePr, &frontBackButtonStyleRel);
	//Create Label for Button
	lv_label_set_text(frontBackLabel, "Front");
	lv_obj_align(frontBackLabel, NULL, LV_ALIGN_IN_TOP_RIGHT, 0, 5);

	//Create Button
	lv_obj_t * frontBackButton = lv_btn_create(lv_scr_act(), NULL);
	lv_cont_set_fit(frontBackButton, true, true); /*Enable resizing horizontally and vertically*/
	lv_obj_align(frontBackButton, frontBackLabel, LV_ALIGN_OUT_BOTTOM_RIGHT, 0, 10);
	lv_obj_set_free_num(frontBackButton, 1);   /*Set a unique number for the button*/
	lv_btn_set_action(frontBackButton, LV_BTN_ACTION_CLICK, toggleFrontBack);
	
	//Set Styles
	lv_btn_set_style(frontBackButton, LV_BTN_STYLE_REL, &frontBackButtonStyleRel);
	lv_btn_set_style(frontBackButton, LV_BTN_STYLE_PR, &frontBackButtonStylePr);

	//Park
	static lv_style_t bg_style;
	static lv_style_t indic_style;
	static lv_style_t knob_on_style;
	static lv_style_t knob_off_style;
	lv_style_copy(&bg_style, &lv_style_pretty);
	bg_style.body.radius = LV_RADIUS_CIRCLE;

	lv_style_copy(&indic_style, &lv_style_pretty_color);
	indic_style.body.radius = LV_RADIUS_CIRCLE;
	indic_style.body.main_color = LV_COLOR_HEX(0x9fc8ef);
	indic_style.body.grad_color = LV_COLOR_HEX(0x9fc8ef);
	indic_style.body.padding.hor = 0;
	indic_style.body.padding.ver = 0;

	lv_style_copy(&knob_off_style, &lv_style_pretty);
	knob_off_style.body.radius = LV_RADIUS_CIRCLE;
	knob_off_style.body.shadow.width = 4;
	knob_off_style.body.shadow.type = LV_SHADOW_BOTTOM;

	lv_style_copy(&knob_on_style, &lv_style_pretty_color);
	knob_on_style.body.radius = LV_RADIUS_CIRCLE;
	knob_on_style.body.shadow.width = 4;
	knob_on_style.body.shadow.type = LV_SHADOW_BOTTOM;

	/*Create a switch and apply the styles*/
	lv_obj_t *sw1 = lv_sw_create(lv_scr_act(), NULL);
	lv_sw_set_style(sw1, LV_SW_STYLE_BG, &bg_style);
	lv_sw_set_style(sw1, LV_SW_STYLE_INDIC, &indic_style);
	lv_sw_set_style(sw1, LV_SW_STYLE_KNOB_ON, &knob_on_style);
	lv_sw_set_style(sw1, LV_SW_STYLE_KNOB_OFF, &knob_off_style);
	lv_obj_align(sw1, NULL, LV_ALIGN_CENTER, 0, -50);
	
	//Create Label for Button
	lv_label_set_text(parkLabel, "Park");
	lv_obj_align(parkLabel, NULL, LV_ALIGN_IN_TOP_MID, 0, 5);
	lv_sw_set_action(sw1, changePark);


}
