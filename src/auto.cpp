#include "auto.hpp"
#include "motor.hpp"
#include "drive.hpp"
#include "sensor.hpp"
#include "angler.hpp"
int valueForOneTurn = 360;
int intakeSped = 120;
int turns = 5 * valueForOneTurn; //Turns for on 1 inch
int convert=4.125*3.1415926535;

void autoMove(float distance, int speed, int timeout){
	int time = pros::millis() + timeout;
	float target;
	float degrees = distance * 23.5;
	target = leftMotor.get_position() + degrees;
	setDriveRelative(degrees,degrees,speed);
	pros::delay(10);
	if (degrees > 0){
		while (leftMotor.get_position() < target && pros::millis() < time) {
			pros::lcd::set_text(2,"I'm done!");
		}
	}
	else if (degrees < 0){
		while (leftMotor.get_position() > target && pros::millis() < time){}
	}
	setDrive(0,0);
}

void autoTurn(float distance, int speed){
	int time = pros::millis() + 5000;
	float degrees = 100;
	if (distance <= 130){
		degrees = distance * (2.37);
	}
	else if (distance >= 130 && distance < 200){
		degrees = distance * 2.15;
	}
	else if (distance >= 200){
		degrees = distance * 2.3;
	}
	float target;
	target = leftMotor.get_position() + degrees;
	setDriveRelative(degrees, -degrees,speed);
	pros::delay(20);
	if (degrees > 0){
		while (leftMotor.get_position() < target && pros::millis() < time) {}
	}
	else if (degrees < 0){
		while (leftMotor.get_position() > target && pros::millis() < time) {}
	}
	setDrive(0,0);
}
void autoIntake(int speed){
	int time = pros::millis() + 10000;
	if (senseIntakeT.get_value() == 0){
		intakeMotor.move(speed);
		while (senseIntakeT.get_value() != 1 && pros::millis() < time) {}
	}
	else if (senseIntakeB.get_value() == 1){
		intakeMotor.move(speed);
		while (senseIntakeB.get_value() != 1 && pros::millis() < time) {}
	}

	intakeMotor.move(0);
}
void autoPunch(){
int time = pros::millis() + 100000;
  puncherMotor.move(127);
  while (senseBall.get_value() < 2600 && pros::millis() < time) {}
  puncherMotor.move(0);
}

//TODO: Function that pulls puncher as far back as possible so that doesn't fire
//This should be called as a task
//https://pros.cs.purdue.edu/v5/tutorials/topical/multitasking.html?highlight=tasks

/*void armPuncher() {
		//TODO AS A TASK WRITE CODE THAT ARMS THE PUNCHER
	puncherMotor = puncherSpeed;
	while(sensePuncher.get_value() < puncherThreshHold) {
		pros::delay(2);
	}
	puncherBeforeThreashHold = puncherMotor.get_position();
	while (puncherMotor.get_position() < puncherBeforeThreashHold + puncherAfterThreashHoldRotate) {
		pros::delay(2);
	}
	puncherMotor = 0;

}*/

//TODO: Loads the puncher by spinning the intake. It should take all the sensors
//account to prevent intaking more than 2 balls and loading more than 1 ball
void loadPuncher() {
	int time = pros::millis() + 100000;
	intakeMotor.move(70);
	while (senseBall.get_value() > 2600 && pros::millis() < time) {
		pros::lcd::print(7, "Now: %d", pros::millis());
		pros::lcd::print(5, "Target: %d", time);
	}
	intakeMotor.move(1);

}
void autoScraper(float degrees){
	int time = pros::millis() + 2000;
	float target;
	target = scraperMotor.get_position() + degrees;
	scraperMotor.move_relative(degrees,200);
	pros::delay(10);
	if (degrees > 0){
		while (scraperMotor.get_position() < target && pros::millis() < time) {}
	}
	else if (degrees < 0){
		while (scraperMotor.get_position() > target && pros::millis() < time){}
	}
}
