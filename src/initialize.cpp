#include "include.hpp"
#include "screen.h"
//#include "red_scare.c"
void on_center_button() { static bool pressed = false;
	pressed = !pressed;
	if (pressed) {
		//pros::lcd::set_text(2, "I was pressed!");
	} else {
		//pros::lcd::clear_line(2);	
		}
}

/**
 * Runs initialization code. This occurs as soon as the program is started.
 *
 * All other competition modes are blocked by initialize; it is recommended
 * to keep execution time for this mode under a few seconds.
 */
void initialize() {
	//1 = Right/Red 2 = Left/Blue
	int autoColor = 1;
	int autoSide = 1;
	int autoMod = 1;
	master.set_text(1,1,"MASTER");
	partner.set_text(1,1,"PARTNER");
//	pros::lcd::initialize();
	//pros::lcd::set_text(0, "Initializing...");
	//pros::lcd::register_btn1_cb(on_center_button);
	intakeMotor.set_brake_mode(pros::E_MOTOR_BRAKE_HOLD);
	//pros::lcd::set_text(0, "Initializing...1");
	scraperMotor.set_brake_mode(pros::E_MOTOR_BRAKE_HOLD);
	//pros::lcd::set_text(0, "Initializing...2");
	puncherMotor.set_brake_mode(pros::E_MOTOR_BRAKE_BRAKE);
	//pros::lcd::set_text(0, "Initializing...3");
	leftMotor.set_brake_mode(pros::E_MOTOR_BRAKE_HOLD);
	leftMotor2.set_brake_mode(pros::E_MOTOR_BRAKE_HOLD);
	rightMotor.set_brake_mode(pros::E_MOTOR_BRAKE_HOLD);
	rightMotor2.set_brake_mode(pros::E_MOTOR_BRAKE_HOLD);

	//senseIntakeT.calibrate();
	//senseBall.calibrate();
	//sensePuncher.calibrate();
	
	start_auton();





}

/**
 * Runs while the robot is in the disabled state of Field Management System or
 * the VEX Competition Switch, following either autonomous or opcontrol. When
 * the robot is enabled, this task will exit.
 */
void disabled() {
	//Something funny on screen here if we get the chance.
	//LV_IMG_DECLARE(red_scare);
        /*lv_obj_t * img1 = lv_img_create(lv_scr_act(), NULL);*/
        //lv_img_set_src(img1, &red_scare);
        //lv_obj_align(img1, NULL, LV_ALIGN_IN_TOP_LEFT, 0, 0);
 
}

/**
 * Runs after initialize(), and before autonomous when connected to the Field
 * Management System or the VEX Competition Switch. This is intended for
 * competition-specific initialization routines, such as an autonomous selector
 * on the LCD.
 *
 * This task will exit when the robot is enabled and autonomous or opcontrol
 * starts.
 */
void competition_initialize() {}
