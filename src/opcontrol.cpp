#include "include.hpp"
#include <string>
#include  "screen.h"
/*Quick Styleguide
Organization Styleguide
	-Extra whitespace line after each new "component"
	-Get controller inputs, then resolve actual motor speeds, then set motors, all
in different places! (this way there is only ever 1 input being given to a motor)
	-Newline before crossing atom's limit line thing, except for comments

Variable Styleguide
	-Controller input variables are <motor>Option
	-Const motor speeds are <motor>Speed
	-What should be sent to the motor is <motor> (we'll call these Finals)
	-Actual motors are called <motor>Motor
	-Define all const variables in .hpp files
	-Finals should ALWAYS be defined in **one** place. NEVER SPLIT FINAL DEFINITIONS!!
*/


void sendIntToScreen (int input){
	std::string screen = std::to_string(input);
	char * cstr = &screen[0u];
//	sendToScreen(cstr);
}
//Timing variables

std::uint32_t now;

std::uint32_t simpleWait;
std::uint32_t searchTime;
std::uint32_t digitalLeftTestTime;


int intakeCounter = 0;
int intakeCutOffClear = 0;

int digitalLeftCounter = 0;
bool activeDigitalLeftSequence = false;

std::uint32_t digitalRightTestTime;
int digitalRightCounter = 0;
bool activeDigitalRightSequence = false;

std::uint32_t digitalATestTime;
int digitalACounter = 0;
bool activeDigitalASequence = false;

std::uint32_t digitalR1TestTime;
int digitalR1Counter = 0;
bool activeDigitalR1Sequence = false;

std::uint32_t digitalXTestTime;
int digitalXCounter = 0;
bool activeDigitalXSequence = false;

std::uint32_t digitalBTestTime;
int digitalBCounter = 0;
bool activeDigitalBSequence = false;

std::uint32_t digitalYTestTime;
int digitalYCounter = 0;
bool activeDigitalYSequence = false;

std::uint32_t startingL1Press;
int oldL1 = 0;

std::uint32_t startingL2Press;
int oldL2 = 0;

//Variables
//Majoirity of Ints in this section might need to be changed into std::uint32_t
bool sequenceRunning = false;
bool reloadingPuncher;
bool singleShotNoReloadSequence = false;
int loadedBallThreashHold = 2500; //Line Sensor Detects ball around 2000, normal light values are around 2800
bool singleShotReloadSequence = false;
std::uint32_t singleShotSequenceTest;
bool shootingPart;
bool targetPuncherFound;
int targetMotorValue;
int puncherReloadTurnAmount = 175;
int acceptableError = 10;
int sensePuncherValue;
bool lookingForBall;
bool initialCutOffFound;
std::uint32_t cutOff;
bool ballFound;
std::uint32_t intakeTime = 300; //Time to intake ball from top
bool doubleShotMacroSequence;
bool reloadSequence;
bool fireAgain;
bool reloadPart;
bool lastDoubleShotMacroSequence;
// bool activeReload;
// bool activeArm;

int left;
int right;
int puncher;
int scraper;
double angler;
int intake;
float distance;

int intakeSpeed = 200;
int scraperSpeed = 127;
int puncherSpeed = 127;
int anglerSpeed = 60;

int leftOption;
int rightOption;
int anglerOption = 0;
int intakeOption;
int scraperOption;
int puncherOption;
int oldIntakeOption = -1;
int oldDgtlLeft = 0;
int lineThresh = 1500;
int fireOption = 0;

int height = 0;
int heightOption;
int newDistance;

int puncherThreshHold = 2700;
int puncherAfterThreashHoldRotate = 270;
int puncherBeforeThreashHold = 0;
int puncherPosition;

int anglerPosition;

int frontLeftPosition;
int backLeftPosition;
int frontRightPosition;
int backRightPosition;

int startingLF;
int startingLB;
int startingRF;
int startingRB;

int startingDistance;
int averageChangeInAngle;
float alternateDistance;

int initialized = 0;
//bool ran = false;
//Main Function
void opcontrol() {
	//Initialization
	int initialTimeout = pros::millis() + 2000;
	while (senseAngler.get_value() < 2200 && pros::millis() < initialTimeout){
		pros::lcd::set_text(0, "Initializing Motor");
		anglerMotor.move(-50);
	}
	anglerMotor.move(0);
	pros::delay(20);
	anglerMotor.tare_position();
	pros::delay(50);
	anglerMotor.set_brake_mode(pros::E_MOTOR_BRAKE_HOLD);
//	sendIntToScreen(anglerMotor.get_position());
	//Control Code
	while (true) {
		//Get motor positions to avoid errors
		puncherPosition = puncherMotor.get_position();
		anglerPosition = anglerMotor.get_position();
		frontLeftPosition = leftMotor.get_position();
		backLeftPosition = leftMotor2.get_position();
		frontRightPosition = rightMotor.get_position();
		backRightPosition = rightMotor2.get_position();
		sensePuncherValue = sensePuncher.get_value();
		now = pros::millis();
		//pros::lcd::print(3,"Current: %d",anglerPosition);
		//LCD Setup
		//pros::lcd::print(3,"Current: %d",anglerPosition);
		//pros::lcd::print(6, "Now: %d", now);
		//Get controller inputs
		//Tank Drive
		leftOption = master.get_analog(ANALOG_LEFT_Y) + 127*master.get_digital(DIGITAL_UP) - 127*master.get_digital(DIGITAL_DOWN);
		rightOption = master.get_analog(ANALOG_RIGHT_Y)+ 127*master.get_digital(DIGITAL_UP) - 127*master.get_digital(DIGITAL_DOWN);


		if (master.get_digital_new_press(DIGITAL_LEFT)) { //A
			activeDigitalLeftSequence = true;
			digitalLeftCounter ++;
			digitalLeftTestTime = now + 200;
		}
		if (activeDigitalLeftSequence) { //Will only be true if DIGITAL_LEFT was pressed recently
			if (now > digitalLeftTestTime) {
				activeDigitalLeftSequence = false;
				switch (digitalLeftCounter) {
					case 1: intakeOption = 1; //Spinning In
						break;
					default: intakeOption = 0;
				}
				digitalLeftCounter = 0;
			}
		}

		if (master.get_digital_new_press(DIGITAL_RIGHT)) { //A
			activeDigitalRightSequence = true;
			digitalRightCounter ++;
			digitalRightTestTime = now + 200;
		}
		if (activeDigitalRightSequence) {
			if (now > digitalRightTestTime) {
				activeDigitalRightSequence = false;
				switch (digitalRightCounter) {
					case 1: intakeOption = -1; //Spinning Out
						break;
					default: intakeOption = 0;
				}
				digitalRightCounter = 0;
			}
		}

		if (master.get_digital_new_press(DIGITAL_A)) {
			activeDigitalASequence = true;
			digitalACounter ++;
			digitalATestTime = now + 200;
		}
		if (activeDigitalASequence) {
			if (now > digitalATestTime) {
				activeDigitalASequence = false;
				switch (digitalACounter) {
					case 1: distance = 36;
						break;
					case 2: distance = 12;
					default: distance = 12;
						break;
				}
				digitalACounter = 0;
			}
		}

		if (master.get_digital_new_press(DIGITAL_X)) { //A
			activeDigitalXSequence = true;
			digitalXCounter ++;
			digitalXTestTime = now + 200;
		}
		if (activeDigitalXSequence) {
			if (now > digitalXTestTime) {
				activeDigitalXSequence = false;
				switch (digitalXCounter) {
					case 1: distance = 60;
						break;
					default: distance = 60;
						break;
				}
				digitalXCounter = 0;
			}
		}

		if (master.get_digital_new_press(DIGITAL_B)) { //A
			activeDigitalBSequence = true;
			digitalBCounter ++;
			digitalBTestTime = now + 200;
		}
		if (activeDigitalBSequence) {
			if (now > digitalBTestTime) {
				activeDigitalBSequence = false;
				switch (digitalBCounter) {
					case 1: distance = 84;
						break;
					default: distance = 84;
						break;
				}
				digitalBCounter = 0;
			}
		}

		if (master.get_digital_new_press(DIGITAL_Y)) {
			activeDigitalYSequence = true;
			digitalYCounter ++;
			digitalYTestTime = now + 200;
		}
		if (activeDigitalYSequence) {
			if (now > digitalYTestTime) {
				activeDigitalYSequence = false;
				switch (digitalYCounter) {
					case 1: distance = 108;
						break;
					case 2: distance = 132;
						break;
					default: distance = 138;
						break;
				}
				digitalYCounter = 0;
			}
		}

		if (master.get_digital_new_press(DIGITAL_R1)) {
			activeDigitalR1Sequence = true;
			digitalR1Counter ++;
			digitalR1TestTime = now + 200;
		}
		if (activeDigitalR1Sequence) {
			if (now > digitalR1TestTime) {
				activeDigitalR1Sequence = false;
				switch (digitalR1Counter) {
						case 1: fireOption = 1; //Singleshot with reload
							break;
						default: fireOption = 2; //Singleshot no reload
							break;
				}
				digitalR1Counter = 0;
			}
		}

		if (master.get_digital_new_press(DIGITAL_R2)) {
			fireOption = 3; //Double Shot
		}

		scraperOption = 0;
		if (master.get_digital_new_press(DIGITAL_L1)) {
			startingL1Press = now;
		}
		if (master.get_digital(DIGITAL_L1)) {
			if (startingL1Press + 200 < now) {
				scraperOption += 1;
			}
		} else if (oldL1 == 1) {
				if (now < startingL1Press + 200) {
					height = 1;
				}
		}

		if (master.get_digital_new_press(DIGITAL_L2)) {
			startingL2Press = now;
		}
		if (master.get_digital(DIGITAL_L2)) {
			if (startingL2Press + 200 < now) {
				scraperOption -= 1;
			}
		} else if (oldL2 == 1) {
				if (now < startingL2Press + 200) {
					height = 0;
				}
		}

		if (senseIntakeT.get_value() == 1 && senseBall.get_value() < loadedBallThreashHold) {
			intakeOption = 0;
		}
		
		if (senseIntakeT.get_value() == 1) {
			intakeCounter += 1;
			intakeCutOffClear = now + 50;
		}
		if (intakeCutOffClear > now) {
			intakeCounter = 0;	
		}
		if (intakeCounter > 1) {
			intakeOption = 0;
		}
		

		//Fire Puncher
		if (fireOption != 0 && !sequenceRunning) {
			if (fireOption == 1) {
				sequenceRunning = true;
				if (senseBall.get_value() < loadedBallThreashHold) {
					singleShotReloadSequence = true;
					shootingPart = true;
					singleShotSequenceTest = now + 200;
					targetPuncherFound = false;
					ballFound = false;
					initialCutOffFound = false;
				} else {
					reloadSequence = true;
					lookingForBall = true;
					ballFound = false;
					initialCutOffFound = false;
				}
			} else if (fireOption == 2) {
				sequenceRunning = true;
				if (senseBall.get_value() < loadedBallThreashHold) {
					singleShotNoReloadSequence = true;
					shootingPart = true;
					targetPuncherFound = false;
				} else {
					reloadSequence = true;
					lookingForBall = true;
					ballFound = false;
					initialCutOffFound = false;
				}
			} else if (fireOption == 3) {
					sequenceRunning = true;
					if (senseBall.get_value() < loadedBallThreashHold) {
						doubleShotMacroSequence = true;
						shootingPart = true;
						targetPuncherFound = false;
					} else {
						reloadSequence = true;
						lookingForBall = true;
						ballFound = false;
						initialCutOffFound = false;
			}
		}
		}
		fireOption = 0;


		if (singleShotNoReloadSequence) {

			if (shootingPart) {
				if (senseBall.get_value() < loadedBallThreashHold) {
					puncherMotor.move_velocity(200);
				} else {
					puncherMotor.move(0);
					shootingPart = false;
					reloadingPuncher = true;
					targetPuncherFound = false;
				}
			} else { //Reload Part
				if (reloadingPuncher) {
					if ((!targetPuncherFound) && (sensePuncherValue < (puncherThreshHold-100))) { //Might need to be a different Threashhold
						puncherMotor.move_velocity(200);
					}
						else if (!targetPuncherFound) {
						targetMotorValue = puncherPosition + 175;
						targetPuncherFound = true;
					} else if (targetMotorValue - puncherPosition > acceptableError) {
						puncherMotor.move_velocity(200);
					} else {
						puncherMotor.move_velocity(0);
						reloadingPuncher = false;
					}
				}

				if (!reloadingPuncher) {
					singleShotNoReloadSequence = false;
					sequenceRunning = false;
				}
			}
		}

		//Single Shot Sequence Resolution
		if (singleShotReloadSequence) {
			if (shootingPart) {
				if (senseBall.get_value() < loadedBallThreashHold) {
					puncherMotor.move_velocity(200);
				} else {
					puncherMotor.move(0);
					shootingPart = false;
					lookingForBall = true;
					ballFound = false;
					reloadingPuncher = true;
					targetPuncherFound = false;
				}
			} else { //Reload Part
				if (reloadingPuncher) {
					if ((!targetPuncherFound) && (sensePuncherValue < (puncherThreshHold-100))) { //Might need to be a different Threashhold
						puncherMotor.move_velocity(200);
					}
						else if (!targetPuncherFound) {
						targetMotorValue = puncherPosition + puncherReloadTurnAmount;
						targetPuncherFound = true;
					} else if (targetMotorValue - puncherPosition > acceptableError) {
						puncherMotor.move_velocity(200);
					} else {
						puncherMotor.move_velocity(0);
						reloadingPuncher = false;
					}
				}

				if (lookingForBall) {
					if (!initialCutOffFound) {
						cutOff = now + 500;
						initialCutOffFound = true;
					}
					if (senseIntakeT.get_value() == 1 && !ballFound) {
							cutOff = now + intakeTime;
							ballFound = true;
						}
					if (cutOff < now) {

						intakeOption = 0;
						lookingForBall = false;
					} else {
						intakeOption = 1;
					}
				}

				if (!reloadingPuncher && !lookingForBall) {
					singleShotReloadSequence = false;
					sequenceRunning = false;

				}
			}
		}

		if (reloadSequence) {
			if (!initialCutOffFound) {
				cutOff = now + 500;
				initialCutOffFound = true;
			}
			if (senseIntakeT.get_value() == 1 && !ballFound) {
					cutOff = now + intakeTime;
					ballFound = true;
				}
			if (cutOff < now) {
				intakeOption = 0;
				lookingForBall = false;
			} else {
				//pros::lcd::print(2, "intaked");
				intakeOption = 1;
			}

			if (!lookingForBall) {
				reloadSequence = false;
				sequenceRunning = false;
			}
		}

		if (doubleShotMacroSequence) {
			if (shootingPart) {
				//pros::lcd::print(1, "P1");
				if (senseBall.get_value() < loadedBallThreashHold) {
					puncherMotor.move_velocity(200);
					//pros::lcd::print(1, "P2");
				} else {
					//pros::lcd::print(1, "P3");
					puncherMotor.move(0);
					shootingPart = false;
					lookingForBall = true;
					ballFound = false;
					reloadPart = true;
					reloadingPuncher = true;
					targetPuncherFound = false;
					initialCutOffFound = false;
					height = abs(height - 1);
				}
			} else if (reloadPart) { //Reload Part
					if (reloadingPuncher) {
					//pros::lcd::print(1, "P5");
					if (!targetPuncherFound && (sensePuncherValue > puncherThreshHold)) { //Might need to be a different Threashhold
						puncherMotor.move_velocity(200);
											//pros::lcd::print(1, "P6");
					}	else if (!targetPuncherFound) {
						targetMotorValue = puncherPosition + puncherReloadTurnAmount + 50;
						targetPuncherFound = true;
											//pros::lcd::print(1, "P7");
					} else if (targetMotorValue - puncherPosition > acceptableError){
						puncherMotor.move_velocity(200);
											//pros::lcd::print(1, "P8");
					} else {
											//pros::lcd::print(1, "P9");
						puncherMotor.move(0);
						reloadingPuncher = false;
					}
				}
				if (lookingForBall) {
					if (!initialCutOffFound) {
						cutOff = now + 1500;
						simpleWait = cutOff + 200;
						initialCutOffFound = true;
					}
					if (senseIntakeT.get_value() == 1 && !ballFound) {
						cutOff = now + intakeTime;
						simpleWait = cutOff + 200;
						ballFound = true;
					}
					if (cutOff < now) {
						intakeOption = 0;
					} else {
						intakeOption = 1;
					}
				}

				if (senseBall.get_value() < loadedBallThreashHold) {
					lookingForBall = false;
				}
				if (simpleWait > now) {
					if ((!reloadingPuncher && !lookingForBall)) {
						if (senseBall.get_value() < loadedBallThreashHold) {
							//pros::lcd::print(3, "Continue");
							reloadPart = false;
							fireAgain = true;
						}
				}
			} else {
				doubleShotMacroSequence = false;
				sequenceRunning = false;
				intakeOption = 0;
				simpleWait = now + 10;
			}
			} else if (fireAgain) {
				if (simpleWait < now) {
					if (senseBall.get_value() < loadedBallThreashHold) {
						puncherMotor.move_velocity(200);
					} else {
						puncherMotor.move(0);
						fireAgain = false;
						reloadingPuncher = true;
						targetPuncherFound = false;
					}
				}
			} else { //Final Arming
				if (reloadingPuncher) {
					//pros::lcd::print(1, "P11");
					if (!targetPuncherFound && (sensePuncherValue > puncherThreshHold)) { //Might need to be a different Threashhold
						puncherMotor.move_velocity(200);
					}	else if (!targetPuncherFound) {
						targetMotorValue = puncherPosition + puncherReloadTurnAmount + 50;
						targetPuncherFound = true;
					} else if (targetMotorValue - puncherPosition > acceptableError ){
						puncherMotor.move_velocity(200);
					} else {
						//pros::lcd::print(1, "Exit");
						puncherMotor.move(0);
						reloadingPuncher = false;
						doubleShotMacroSequence = false;
						sequenceRunning = false;
					}
				}
			}
		}

		if (!lastDoubleShotMacroSequence && doubleShotMacroSequence) {
			startingLF = frontLeftPosition;
			startingLB = backLeftPosition;
			startingRF = frontRightPosition;
			startingRB = backRightPosition;
			startingDistance = distance;
		}
		if (doubleShotMacroSequence) {
			averageChangeInAngle = (((frontLeftPosition - startingLF) +
			(backLeftPosition - startingLB) + (frontRightPosition - startingRF) +
			(backRightPosition - startingRB)) / 4);
			alternateDistance = startingDistance - 12.566*(averageChangeInAngle/360);

		}

		//Basic Speed Resolution
		left = leftOption;
		right = rightOption;
		scraper = scraperOption*scraperSpeed;
		angler =  anglerOption*anglerSpeed;
		puncher = puncherSpeed * puncherOption;
		intake = intakeSpeed * intakeOption;

		//Set Motors
		setDrive(left, right);

		//puncherMotor.move(puncher);
		scraperMotor.move(scraper);
		//anglerMotor.move(angler);
		if (intakeOption == 1 && sequenceRunning == true){
			intakeMotor.move_velocity(intake*.8);
		}
		else{
			intakeMotor.move_velocity(intake);
		}

		//Sets angler to move if a new distance has been input, and checks for Height.
		if (doubleShotMacroSequence) {
			if (height == 1){
				setAngleHigh(distance);
			}
			else if(height == 0){
				setAngleLow(distance);
			}
		} else {
			if (height == 1){
				setAngleHigh(distance);
			}
			else if(height == 0){
				setAngleLow(distance);
			}
		}
		//Misc End Stuff
		//pros::lcd::print(0, "Sequence Running: %d. Fire Opt: %d", sequenceRunning, fireOption);

		//// pros::lcd::print(0, "%d", intakeOption);
		// if (activeDigitalASequence) {
		//// 	pros::lcd::print(1, "True");
		// } else {
		//// 	pros::lcd::print(1, "False");
		// }
		//Old Variables
		oldL1 = master.get_digital(DIGITAL_L1); 
		oldL2 = master.get_digital(DIGITAL_L2);
		oldIntakeOption = intakeOption;
		lastDoubleShotMacroSequence = doubleShotMacroSequence;

		//Secondary Controller Overrides
		if (partner.get_digital(DIGITAL_X) == 1){
			sequenceRunning = false;
			intakeMotor.move(0);
			anglerMotor.move(0);
			scraperMotor.move(0);


		}
		if (partner.get_digital(DIGITAL_DOWN) == 1){
			while (senseAngler.get_value() < 2200){
				//pros::lcd::set_text(0, "Initializing Motor");
				anglerMotor.move(-50);
			}
			anglerMotor.move(0);
			pros::delay(20);
			anglerMotor.tare_position();
			pros::delay(50);
			anglerMotor.set_brake_mode(pros::E_MOTOR_BRAKE_HOLD);
		}
		pros::delay(10);

	}
}
