#include "include.hpp"
#include <string>
#include "autonVars.hpp"
#include "screen.h"
/**
 * Runs the user autonomous code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the autonomous
 * mode. Alternativelys, this function may be called in initialize or opcontrol
 * for non-competition testing purposes.
 *
 * If the robot is disabled or communications is lost, the autonomous task
 * will be stopped. Re-enabling the robot will restart the task, not re-start it
 * from where it left off.
 */


int moveTimeout = 2500;
int moveSpeed = 95; //85 works
int turnSpeed = 85;
pros::Mutex scraperMutex;
bool doTask = true;
int stop;
void scraperTaskFn(void* param){
	while(true){
		if(scraperMutex.take(999999) && doTask){
			autoScraper(150);
			autoScraper(-150);
			doTask = false;
		}
		scraperMutex.give();
	}
}

void autonomous() { // Initialize
	int shootTime = pros::millis() + 13300;
	scraperMutex.take(100);
	pros::Task scraperTask(scraperTaskFn, (void*)"PROS", TASK_PRIORITY_DEFAULT, TASK_STACK_DEPTH_DEFAULT, "My Task");
	//sendToScreen("HI!");
	int initialTimeout = pros::millis() + 2000;
	while (senseAngler.get_value() < 2200 && pros::millis() < initialTimeout){
		//pros::lcd::set_text(0, "Initializing Motor");
		anglerMotor.move(-50);
	}
	leftMotor.set_brake_mode(pros::E_MOTOR_BRAKE_HOLD);
	leftMotor2.set_brake_mode(pros::E_MOTOR_BRAKE_HOLD);
	rightMotor.set_brake_mode(pros::E_MOTOR_BRAKE_HOLD);
	rightMotor2.set_brake_mode(pros::E_MOTOR_BRAKE_HOLD);
	anglerMotor.move(0);
	pros::delay(20);
	anglerMotor.tare_position();
	pros::delay(50);
	anglerMotor.set_brake_mode(pros::E_MOTOR_BRAKE_HOLD);
	// Auton Proper
	// Front
	if (auton::frontBack){
	//	sendToScreen("Front");
		//Deploy
		scraperMutex.give(); //Get ball
		intakeMotor.move(200);
		autoMove(38,150, moveTimeout);
		autoIntake(200);
		//Flip Cap
		autoMove(-24,moveSpeed*1.5, moveTimeout);
		pros::delay(100);
		autoTurn(100.0*auton::redBlue,turnSpeed);
		autoMove(-8,moveSpeed, moveTimeout);
		scraperMutex.take(500);
		autoScraper(360*2*.6*.80);
		autoScraper(-360*2*.6*.80); //Align
		autoMove(8,moveSpeed,moveTimeout);
		autoTurn(100*auton::redBlue,turnSpeed);
		autoMove(32,moveSpeed*3*1.5,moveTimeout);
		autoMove(-7,moveSpeed,moveTimeout);
		pros::delay(100);
		//Hit Low FLag
		autoTurn(90*auton::redBlue,turnSpeed);
		pros::delay(100);
		autoMove(50,140,moveTimeout);
		pros::delay(100);
		autoMove(-50,140,moveTimeout);
		//autoMove(48,moveSpeed); //SHOULD BE 48
		//Double Shot
		//autoMove(-24,moveSpeed); //SHOULD BE 48
		pros::delay(100);
		autoTurn(5*auton::redBlue,turnSpeed); 
		//Add wait here
		setAngleHigh(40);
		while(pros::millis() < shootTime){}  //Wait
		autoPunch();
		loadPuncher();
		setAngleLow(40);
		autoPunch();


	}
	//Back
	else{
		//sendToScreen("Back");
		//Get Ball
		intakeMotor.move(200);
		scraperMutex.give();
		autoMove(38, 150,moveTimeout);
		autoIntake(200);
		// Flip Cap
		autoTurn(-100*auton::redBlue, turnSpeed);
		pros::delay(100);
		autoMove(-8, moveSpeed,moveTimeout);
		while(scraperMutex.take(100));
		autoScraper(360*2);
		autoScraper(-360*2);
		// Angle Setup
		//autoTurn(105*auton::redBlue, turnSpeed);
		if (auton::park == true){
			//Park
			pros::delay(200);
		//	autoMove(-8,moveSpeed*2,moveTimeout);
		//	autoTurn(-100*auton::redBlue, turnSpeed);
		//	pros::delay(100);
			autoMove(46, 125,moveTimeout);
			pros::delay(200);
			autoMove(-4,30,moveTimeout);
			autoTurn(90*auton::redBlue, turnSpeed);
			autoMove(7.9,70,moveTimeout/2);
		if(auton::redBlue == -1){
			autoMove(-6,30,moveTimeout);
			autoTurn(-45 *auton::redBlue, 50);
			}
		else{
				autoMove(-8,30,moveTimeout);
				autoTurn(-47 *auton::redBlue, 50);//
			}

			anglerMotor.move_absolute(92, 200);
			while(pros::millis() < shootTime){}
			autoPunch();
			intakeMotor.move(200);
			loadPuncher();
			anglerMotor.move_absolute(160, 200);
			//setAngleLow(114); //104 is flex 114 is normal
			autoPunch();
		}
		else{
			//No Park
			pros::delay(200);
			autoMove(-15, moveSpeed, moveTimeout);
			autoTurn(100*auton::redBlue, turnSpeed);
			pros::delay(100);
			autoMove(20, moveSpeed, moveTimeout);
			pros::delay(200);
			autoMove(-6, moveSpeed, moveTimeout);
			autoTurn(8.7*1.5*auton::redBlue, turnSpeed);
			//Fire Puncher
			anglerMotor.move_absolute(90,200);
			pros::delay(200);
			autoPunch();
			pros::delay(200);
			loadPuncher();
			pros::delay(200);
			setAngleLow(93);
			pros::delay(200);
			autoPunch();
			pros::delay(200);
		}
	}
}
