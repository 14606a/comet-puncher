#include "motor.hpp"
#include "drive.hpp"

void setDrive(int left, int right){
  leftMotor.move(left);
  leftMotor2.move(left);
  rightMotor.move(right);
  rightMotor2.move(right);
}

void setDriveRelative(int left, int right, int speed){
	leftMotor.move_relative(left,speed);
	leftMotor2.move_relative(left,speed);
	rightMotor.move_relative(right,speed);
	rightMotor2.move_relative(right,speed);
}
void setMotorPID(pros::motor_pid_s_t pid){
	leftMotor.set_pos_pid(pid);
	leftMotor2.set_pos_pid(pid);
	rightMotor.set_pos_pid(pid);
	rightMotor2.set_pos_pid(pid);
}
/*
void setMotorBrake(bool x){
	if (x){
		pros::jj	
	leftMotor.set_brake_mode(p
	*/
