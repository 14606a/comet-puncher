#include "main.h"
#include "motor.hpp"
//Controllers
pros::Controller master(pros::E_CONTROLLER_MASTER);
pros::Controller partner(pros::E_CONTROLLER_PARTNER);
//Motors
pros::Motor leftMotor(10,pros::E_MOTOR_GEARSET_18,0,pros::E_MOTOR_ENCODER_DEGREES);
pros::Motor leftMotor2(4,pros::E_MOTOR_GEARSET_18,0,pros::E_MOTOR_ENCODER_DEGREES); //Back
pros::Motor rightMotor(9,pros::E_MOTOR_GEARSET_18,1,pros::E_MOTOR_ENCODER_DEGREES);
pros::Motor rightMotor2(1,pros::E_MOTOR_GEARSET_18,1,pros::E_MOTOR_ENCODER_DEGREES); //Back
pros::Motor intakeMotor(5,pros::E_MOTOR_GEARSET_18,1,pros::E_MOTOR_ENCODER_DEGREES);
pros::Motor puncherMotor(14,pros::E_MOTOR_GEARSET_18,1,pros::E_MOTOR_ENCODER_DEGREES);
pros::Motor scraperMotor(6,pros::E_MOTOR_GEARSET_18,0,pros::E_MOTOR_ENCODER_DEGREES);
pros::Motor anglerMotor(12,pros::E_MOTOR_GEARSET_18,0,pros::E_MOTOR_ENCODER_DEGREES);
