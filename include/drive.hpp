#ifndef _DRIVE_H_
#define _DRIVE_H_
// Sets the speeds of the left and right wheels of the chassis
void setDrive(int left, int right);
void setDriveRelative(int left, int right, int speed);
void setMotorPID(pros::motor_pid_s_t pid);
#endif // _DRIVE_H_
