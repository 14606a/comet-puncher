#include "main.h"
#ifndef _auto_H_
#define _auto_H_
// Sets the speeds of the left and right wheels of the chassis
void scraperTaskFn(void* param);
void autoMove(float distance, int speed,int timeout);
void autoTurn(float distance, int speed);
void autoIntake(int speed);
void autoPunch();
void loadPuncher();
void autoLift(float speed, int time);

void autoScraper(float degrees);
#endif // _auto_H_
