#include "main.h"
#ifndef _SENSORS_H_
#define _SENSORS_H_
/* The following sensors will be used:
 Line followers:
 	Bottom of Intake
	Near Top of Intake
	Ball slot on puncher
	Back of puncher to detect if pulled
	*/
extern pros::ADILineSensor senseIntakeB;
extern pros::ADIDigitalIn senseIntakeT;
extern pros::ADILineSensor senseBall;
extern pros::ADILineSensor sensePuncher;
extern pros::ADIAnalogIn senseAngler;
#endif // _SENSORS_H_
