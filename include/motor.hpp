#include "main.h"
#ifndef _MOTOR_H_
#define _MOTOR_H_
// Defines Controller
extern pros::Controller master;
extern pros::Controller partner;

// Defines motor
extern pros::Motor leftMotor;
extern pros::Motor leftMotor2;
extern pros::Motor rightMotor;
extern pros::Motor rightMotor2;
extern pros::Motor intakeMotor;
extern pros::Motor puncherMotor;
extern pros::Motor scraperMotor;
extern pros::Motor anglerMotor;
#endif // _MOTOR_H_
